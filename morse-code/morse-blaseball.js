console.log("Hello, world");

const BLASEBALL_ROOT = "https://before.sibr.dev";
// const STREAM_SOURCE = `${BLASEBALL_ROOT}/events/streamData`;
const STREAM_SOURCE = "https://api.sibr.dev/replay/v1/replay?from=2020-08-30T01:00:08.17Z"

var playing = true;

var m = new jscw({"wpm": 55});

function playMorse() {
    m.play("Hello, world")
}

function play() {
    playing = true;
}

function stop() {
    playing = false;
    m.stop();
}

(() => {
    var stream = new EventSource(STREAM_SOURCE);
    stream.addEventListener("message", (event) => {
        const games = JSON.parse(event.data).value?.games?.schedule
        // console.log(games);
        console.log(games[0].lastUpdate)
        let msg = games[0].lastUpdate;
        if (!msg.endsWith(".")) {
            msg += "."
        }
        if (playing) {
            if (m.getRemaining() == 0) {
                m.play(msg)
            }
            else
            {
                console.warn("Missed: ", msg)
            }
        }
    });
})()
